#!/bin/sh

# Set the tty number
tty="/dev/tty2"

# Get the hour (0-23)
hour=`date +%H`

# Default to day theme
file="day"
# Night theme from 18:00-06:00
if [ ${hour} -lt 6 ] || [ ${hour} -gt 18 ]
then
	file="night"
fi

# Echo the colors to $tty
count=0
for line in `cat ${file}`
do
	# Must use printf's absolute path; dash's printf doesn't work
	`/usr/bin/printf "\e]P%x%s" ${count} ${line} > ${tty}`
	count=`expr ${count} + 1`
done

# If tmux is running, refresh the client running in $tty
pgrep tmux > /dev/null && tmux refresh-client -t${tty}
